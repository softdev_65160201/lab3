/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXUnitTest1 {
    
    public OXUnitTest1() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow2_O_output_true(){
        String[][] table = {{"_","_","_"},{"O","O","O"},{"_","_","_"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow1_O_output_true(){
        String[][] table = {{"O","O","O"},{"_","_","_"},{"_","_","_"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow3_O_output_true(){
        String[][] table = {{"_","_","_"},{"_","_","_"},{"O","O","O"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow3_O_output_false(){
        String[][] table = {{"_","_","_"},{"_","_","_"},{"O","O","_"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinCol1_X_output_true(){
        String[][] table = {{"X","_","_"},{"X","_","_"},{"X","O","_"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol2_X_output_true(){
        String[][] table = {{"_","X","_"},{"_","X","_"},{"_","X","_"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol3_X_output_false(){
        String[][] table = {{"_","X","_"},{"_","_","X"},{"_","_","X"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinDiagonal1_O_output_true(){
        String[][] table = {{"O","_","_"},{"_","O","_"},{"_","_","O"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinDiagonal2_X_output_true(){
        String[][] table = {{"_","_","X"},{"_","X","_"},{"X","_","_"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    public void testCheckWinDiagonal3_X_output_False(){
        String[][] table = {{"_","_","X"},{"_","X","_"},{"_","X","_"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(false,result);
    }    
}
